package abb.abbtechbookstorenotification.repository;

import abb.abbtechbookstorenotification.entity.Notification;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface NotificationRepo extends JpaRepository<Notification, UUID> {


}
