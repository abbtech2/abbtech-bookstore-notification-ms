package abb.abbtechbookstorenotification.controller;

import abb.abbtechbookstorenotification.dto.NotificationDto;
import abb.abbtechbookstorenotification.service.NotificationService;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/notification")
@RequiredArgsConstructor
@Tag(name = "Notification ms ",description = "My Notification  Controller")
@Slf4j
public class NotificationController {

    private final NotificationService notificationService;


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void send(@Valid @RequestBody NotificationDto notificationRequestDto) {
        notificationService.send(notificationRequestDto);
    }

    @GetMapping
    public List<NotificationDto> getAll() {
        return notificationService.getAllNotification();
    }



}
