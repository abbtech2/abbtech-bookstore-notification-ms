package abb.abbtechbookstorenotification.mapper;

import abb.abbtechbookstorenotification.dto.NotificationDto;
import abb.abbtechbookstorenotification.entity.Notification;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface NotificationMapper {

    NotificationDto mapNotificationToDto(Notification notification);

    Notification mapDtoToNotification(NotificationDto notificationDto);
}
