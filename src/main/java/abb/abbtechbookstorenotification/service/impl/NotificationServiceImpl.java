package abb.abbtechbookstorenotification.service.impl;

import abb.abbtechbookstorenotification.dto.NotificationDto;
import abb.abbtechbookstorenotification.entity.Notification;
import abb.abbtechbookstorenotification.mapper.NotificationMapper;
import abb.abbtechbookstorenotification.repository.NotificationRepo;
import abb.abbtechbookstorenotification.service.NotificationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class NotificationServiceImpl implements NotificationService {

    private final NotificationRepo notificationRepo;
    private final NotificationMapper mapper;


    @Override
    public List<NotificationDto> getAllNotification() {
        return notificationRepo.findAll().stream()
                .map(mapper::mapNotificationToDto)
                .toList();
    }

    @Override
    @KafkaListener(topics = "order-confirmation", groupId = "my-group")
    public void send(NotificationDto notificationReqDto) {
        log.info("Sending notification: {}", notificationReqDto);

        Notification notification = new Notification();
        notification.setMessage("Your order has been ");
        notification.setUserEmail(notificationReqDto.getUserEmail());
        notification.setUserId(notificationReqDto.getUserId());
        notification.setOrderId(notificationReqDto.getOrderId());

        notificationRepo.save(notification);

    }


}
