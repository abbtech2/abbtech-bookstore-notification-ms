package abb.abbtechbookstorenotification.service;

import abb.abbtechbookstorenotification.dto.NotificationDto;
import org.springframework.kafka.annotation.KafkaListener;

import java.util.List;

public interface NotificationService {

    List<NotificationDto> getAllNotification();

    @KafkaListener(topics = "order-confirmation", groupId = "my-group")
    void send(NotificationDto notificationReqDto);

}
